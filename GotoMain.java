import soot.*;
import soot.jimple.*;
import soot.util.*;
import java.io.*;
import java.util.*;

public class GotoMain
{    
    public static void main(String[] args) 
    {
        if(args.length == 0)
        {
            System.out.println("Usage: java MyAnalysis class_to_analyse");
            System.exit(0);
        }            
        else
		{
			System.out.println("[mainClass]"+args[0]);
		}
        PackManager.v().getPack("jtp").add(new Transform("jtp.instrumenter", new MyAnalysis()));

        Scene.v().addBasicClass("java.io.PrintStream",SootClass.SIGNATURES);
        Scene.v().addBasicClass("java.lang.System",SootClass.SIGNATURES);
        soot.Main.main(args);
    }
}

/**
    MyAnalysis codes to realize internalTransform interface
   
 */

class MyAnalysis extends BodyTransformer
{
    private boolean isAdded = false;
    private SootClass javaIoPrintStream;

	private void outputBeforeEnd(Boolean initiated, Body myBody, Chain myUnits, Stmt sentence, SootField gotoNumber, Local tmpIO, Local tmpCounter)
    {
		if (!initiated)
		{ 
			tmpIO = Jimple.v().newLocal("tmpIO", RefType.v("java.io.PrintStream"));
			myBody.getLocals().add(tmpIO);
			tmpCounter = Jimple.v().newLocal("tmpCounter", LongType.v()); 
			myBody.getLocals().add(tmpCounter);			
			initiated = true;
		}
		myUnits.insertBefore(Jimple.v().newAssignStmt(tmpIO, Jimple.v().newStaticFieldRef( 
                      Scene.v().getField("<java.lang.System: java.io.PrintStream out>").makeRef())), sentence);
        // insert "tmpCounter = gotoNumber;" 
        myUnits.insertBefore(Jimple.v().newAssignStmt(tmpCounter,Jimple.v().newStaticFieldRef(gotoNumber.makeRef())), sentence);
		// insert "tmpIO.println("Amount of goto sentence is ");" 
        myUnits.insertBefore(Jimple.v().newInvokeStmt(Jimple.v().newVirtualInvokeExpr(tmpIO, javaIoPrintStream.getMethod("void print(java.lang.String)").makeRef(),StringConstant.v("Amount of goto sentence is "))), sentence);
        // insert "tmpIO.println(tmpCounter);"      
        myUnits.insertBefore(Jimple.v().newInvokeStmt(
                      Jimple.v().newVirtualInvokeExpr(tmpIO, javaIoPrintStream.getMethod("void println(long)").makeRef(), tmpCounter)), sentence);
	}
	
    protected void internalTransform(Body myBody, String phaseName, Map options)
    {
        SootClass sClass = myBody.getMethod().getDeclaringClass();
        SootField gotoNumber = null;
        boolean addedLocals = false;
        Local tmpIO = null, tmpCounter = null;
		if (!Scene.v().getMainClass().declaresMethod("void main(java.lang.String[])"))
			throw new RuntimeException("There is no main() in mainClass");
		if (isAdded)
			gotoNumber = Scene.v().getMainClass().getFieldByName("gotoNumber");
		else
		{	// Add gotoNumber field
			gotoNumber = new SootField("gotoNumber", LongType.v(),Modifier.STATIC);
			Scene.v().getMainClass().addField(gotoNumber);
			javaIoPrintStream = Scene.v().getSootClass("java.io.PrintStream");
			isAdded = true;
		}
		boolean isMainMethod = myBody.getMethod().getSubSignature().equals("void main(java.lang.String[])");
		Local tmpLocal = Jimple.v().newLocal("tmp", LongType.v());
		myBody.getLocals().add(tmpLocal);
		Chain myUnits = myBody.getUnits();    
		Iterator stmts = myUnits.snapshotIterator();
		while(stmts.hasNext())
		{
			Stmt sentence = (Stmt) stmts.next();
			if(sentence instanceof GotoStmt)
			{
				// insert "tmpLocal = gotoNumber;"
				myUnits.insertBefore(Jimple.v().newAssignStmt(tmpLocal,Jimple.v().newStaticFieldRef(gotoNumber.makeRef())), sentence);
				// insert "tmpLocal = tmpLocal + 1L;" 
				myUnits.insertBefore(Jimple.v().newAssignStmt(tmpLocal,Jimple.v().newAddExpr(tmpLocal, LongConstant.v(1L))), sentence);
				// insert "gotoNumber = tmpLocal;" 
				myUnits.insertBefore(Jimple.v().newAssignStmt(Jimple.v().newStaticFieldRef(gotoNumber.makeRef()), tmpLocal), sentence);
				System.out.println("This is a goto sentence.");
			}
			else if (sentence instanceof InvokeStmt)
			{
				InvokeExpr iexpr = (InvokeExpr) ((InvokeStmt)sentence).getInvokeExpr();
				if (iexpr instanceof StaticInvokeExpr)
				{
					SootMethod target = ((StaticInvokeExpr)iexpr).getMethod();					
					if (target.getSignature().equals("<java.lang.System: void exit(int)>"))
					{
						outputBeforeEnd(addedLocals, myBody, myUnits, sentence, gotoNumber, tmpIO, tmpCounter);
					}
				}
			}
			else if (isMainMethod && (sentence instanceof ReturnStmt || sentence instanceof ReturnVoidStmt))
			{
				outputBeforeEnd(addedLocals, myBody, myUnits, sentence, gotoNumber, tmpIO, tmpCounter);				
			}
		}
   
    }
}